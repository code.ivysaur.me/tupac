# tupac

![](https://img.shields.io/badge/written%20in-PHP-blue)

A set of functions to pack multiple ints into one.

## Changelog

2015-02-08 r07
- Add MODE_HIBASE algorithm
- [⬇️ tupac_r07.tar.gz](dist-archive/tupac_r07.tar.gz) *(1.52 KiB)*


2015-02-06 r02
- Initial release
- [⬇️ tupac_r02.tar.gz](dist-archive/tupac_r02.tar.gz) *(1.36 KiB)*

